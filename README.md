The files are all collected on the internet and maybe not official documents from the company which they should belong to. I am not responsable for the integrity and reality of the files collected.
# How to download the release notes?
The files are stored by [Company]/[Software Name]/[Major Version]/[Minor Versions]/[Build or Beta version], download them by visiting the directories.
## Share by direct link
Go to the file page in GitLab, copy the GitLab address and replace "https://gitlab.com/chrischan514/Release-Notes/blob" of the address with "https://rn.chrischan514.com", and you will get the direct link.
# How to upload files to make the repo have more release notes collected?
Refer to the contribution guide.
# What kind of release notes has been collected?
The release notes collected now are:  
  
## Apple
- iOS
- watchOS
- tvOS
- macOS
- Xcode
- Shortcut
